// MxN    NxK     MxK
//
// ***           *****
// +++   +****   +****
// *** x +**** = *****
// ***   +****   *****
// ***           *****
// 
use std::time::Instant;

pub struct Matrix {
    pub data: Vec<f32>,
    pub rows: usize,
    pub columns: usize
}

impl Matrix {
    pub fn fill(value: f32, rows: usize, columns: usize) -> Self {
        Self {
            data: vec![value; rows * columns],
            rows,
            columns
        }
    }

    pub fn len(&self) -> usize {
        self.rows * self.columns
    }
}

pub fn measure(size: usize) {
    let left = Matrix::fill(1f32, size, size);
    let right = Matrix::fill(1f32, size, size);
    let mut result = Matrix::fill(0f32, size, size);

    let start = Instant::now();
    unsafe {
        dot_transposed(&left, &right, &mut result);
    }
    let duration = start.elapsed();
    let time = duration.as_nanos() as f32;
    let size = size as f32;
    let timems = (time / 1e6) as u64;
    let gflop = 2f32 * (size * size * size) * 1e-9;
    let gflops = gflop / (time as f32);  
    println!("{gflop} GFLOP executed in {timems} ms ({gflops} GLOP/S)"); 

    let result_sum = sum(&result);
    assert_eq!(result_sum, (size * size * size) as f32, "Invalid sum!");
}

const BLOCK: usize = 4;
#[inline(never)]
unsafe fn dot_transposed(left: &Matrix, right: &Matrix, result: &mut Matrix) {
    let result = &mut result.data;
    for left_row in 0..left.rows {
        for right_column in 0..right.columns {
            let mut acc_result = 0f32;
            for left_column in (0..left.columns).step_by(BLOCK) {
                let mut acc = [0f32; BLOCK];
                for block_column in 0..BLOCK {
                    let left_index = left_row * left.columns + left_column + block_column;
                    let left = left.data.get_unchecked(left_index);
                    acc[block_column] = *left;
                }
                for block_column in 0..BLOCK {
                    let right_index = left_row * right.columns + left_column + block_column;
                    let right = right.data.get_unchecked(right_index);

                    acc[block_column] *= *right;
                }
                acc_result += acc.iter().sum::<f32>();
            }
            let result_index = left_row * right.rows + right_column;
            result[result_index] = acc_result;
        }
    }
}

fn sum(matrix: &Matrix) -> f32 {
    matrix.data.iter().sum()
}

fn main() {
    let size = 16;
    for _ in 0..1 {
        measure(size);
    }
    let width = 1280;
    let height = 1024;
    let input = Matrix::fill(1.0, width, height);
    println!("Input sum before correct  {}", sum(&input));
    for _ in 0..100 {
        let start = Instant::now();
        sum(&input);
        let finish = start.elapsed().as_nanos() as f32 / 1e6;
        println!("Sum time: {}", finish);
    }
    println!("Input sum after correct  {}", sum(&input));
}
