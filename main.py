#!/usr/bin/python3

import os

os.environ["OMP_NUM_THREADS"] = "1"

import numpy as np
from time import perf_counter


SIZE = 2048

def measure():
    shape = (SIZE, SIZE)
    a = np.ones(shape, dtype=np.float32)
    b = np.ones(shape, dtype=np.float32)
    start = perf_counter()
    _ = np.dot(a, b)
    stop = perf_counter()
    sec = stop - start
    flop = 2 * SIZE * SIZE * SIZE
    gflop = flop * 1e-9
    gflops = flop / sec * 1e-9
    print(f"{gflop} GFLOP executed in {int(sec * 1000)} ms: {gflops} GFLOPS")

def main():
    for _ in range(5):
        measure()

if __name__ == "__main__":
    main()
